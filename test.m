close all;

%% Param�tres
borneA = -25;
borneB = 25;
nombreEchantillion = 65536;
periodeEchantillionnage = (borneB-borneA)/nombreEchantillion;
frequenceEchantillionage = 1/periodeEchantillionnage;

%% Calcul abscisse
x = -25:periodeEchantillionnage:25-periodeEchantillionnage;
Xfreq = -frequenceEchantillionage/2:frequenceEchantillionage/nombreEchantillion:frequenceEchantillionage/2-frequenceEchantillionage/nombreEchantillion;

%% Trucs utiles
U = 0.5*sign(x)+0.5; % Echelon unit�
% On utilisera en fait : heaviside(x)

%% Calcul fonction
nombrePerdiodeIntervalle = 5.4; %Nombre entier 0,1,2,3, ...
f0 = nombrePerdiodeIntervalle/(borneB-borneA);
%f0 = 20; % Pour le d�finir � la main
beta = 1;
thau = 0.05;

y1 = cos(2*pi*f0*x);
y2 = sin(2*pi*f0*x);
y3 = heaviside(x+(thau/2))-heaviside(x-(thau/2)); % Normalement : (2016b) rectangularPulse(-thau,thau,x);
y4 = exp(-beta*x).*heaviside(x);
y5 = exp(i*2*pi*f0*x);
y6 = exp(-pi*power(x,2));

deltaF = 20;
f = 300;
g300 =  sin(2*pi*(f-deltaF)*x) + sin(2*pi*f*x) + 2*sin(2*pi*(f+deltaF)*x);
f = 1000;
g1000 =  sin(2*pi*(f-deltaF)*x) + sin(2*pi*f*x) + 2*sin(2*pi*(f+deltaF)*x);

Im1 = zeros(512);
Im2 = zeros(512);
Im3 = zeros(512);
Im4 = zeros(512);

for i = 1:512
    for j= 1:512
        r = sqrt(i*i + j*j);
        Im1(i,j) = 128+120*cos(2*pi*power(10,-5)*r*r);
        Im2(i,j) = 128+120*cos(2*pi*power(10,-4)*r*r);
        Im3(i,j) = 128+120*cos(2*pi*power(10,-3)*r*r);
        Im4(i,j) = 128+120*cos(2*pi*power(10,-2)*r*r);
    end
end

%% Calcul transform�es 
Y1 = tfour(y1);
Y2 = tfour(y2);
Y3 = tfour(y3);
Y4 = tfour(y4);
Y5 = tfour(y5);
Y6 = tfour(y6);
Y6Theorique = pi*exp(-power(2*pi*Xfreq,2)/(4*pi));

G300 = tfour(g300);
G1000 = tfour(g1000);

%% Calcul des transform�es inverses
x1recalcule = tfourinv(Y1);
x2recalcule = tfourinv(Y2);
x3recalcule = tfourinv(Y3);
x4recalcule = tfourinv(Y4);
x5recalcule = tfourinv(Y5);
x6recalcule = tfourinv(Y6);

g300recalcule = tfourinv(G300);
g1000recalcule = tfourinv(G1000);

%% Affichage des fonctions
figure % opens new figure window
title('Affichage des fonctions de bases')
nbLigne1 = 2;
nbColon1 = 3;
subplot(nbLigne1,nbColon1,1); plot(x,real(y1),x,imag(y1)); title('Cosinus'); xlabel('temps'); ylabel('amplitude'); legend('r�elle','imaginaire');
subplot(nbLigne1,nbColon1,2); plot(x,real(y2),x,imag(y2)); title('Sinus'); xlabel('temps'); ylabel('amplitude'); legend('r�elle','imaginaire');
subplot(nbLigne1,nbColon1,3); plot(x,real(y3),x,imag(y3)); title('Rectangle (Thau)'); xlabel('temps'); ylabel('amplitude'); legend('r�elle','imaginaire');
subplot(nbLigne1,nbColon1,4); plot(x,real(y4),x,imag(y4)); title('Exp avec �chelon unit�'); xlabel('temps'); ylabel('amplitude'); legend('r�elle','imaginaire');
subplot(nbLigne1,nbColon1,5); plot(x,real(y5),x,imag(y5)); title('Exp complexe'); xlabel('temps'); ylabel('amplitude'); legend('r�elle','imaginaire');
subplot(nbLigne1,nbColon1,6); plot(x,real(y6),x,imag(y6)); title('Gausienne'); xlabel('temps'); ylabel('amplitude'); legend('r�elle','imaginaire');

%% Affichage des transform�es (r�elles)
figure
title('Affichage des transform�es')
nbLigne2 = 2;
nbColon2 = 3;
subplot(nbLigne2,nbColon2,1); plot(Xfreq,real(Y1), Xfreq,imag(Y1)); title('Cosinus'); xlabel('fr�quence'); ylabel('amplitude'); legend('r�elle','imaginaire');
subplot(nbLigne2,nbColon2,2); plot(Xfreq,real(Y2), Xfreq,imag(Y2)); title('Sinus'); legend('r�elle','imaginaire');
subplot(nbLigne2,nbColon2,3); plot(Xfreq,real(Y3), Xfreq,imag(Y3)); title('Rectangle (Thau)'); legend('r�elle','imaginaire');
subplot(nbLigne2,nbColon2,4); plot(Xfreq,real(Y4), Xfreq,imag(Y4)); title('Exp avec �chelon unit�'); legend('r�elle','imaginaire');
subplot(nbLigne2,nbColon2,5); plot(Xfreq,real(Y5), Xfreq,imag(Y5)); title('Exp complexe'); legend('r�elle','imaginaire');
subplot(nbLigne2,nbColon2,6); plot(Xfreq,real(Y6), Xfreq,imag(Y6)); title('Gausienne'); legend('r�elle','imaginaire');

%% Affichage des amplitude et phase
figure
title('Affichage des transform�es (amplitude/phase)')
nbLigne3 = 3;
nbColon3 = 4;
subplot(nbLigne3,nbColon3,1); plot(Xfreq,abs(Y1)); title('Cosinus Amplitude'); xlabel('fr�quence'); ylabel('amplitude');
subplot(nbLigne3,nbColon3,2); plot(Xfreq,angle(Y1)); title('Cosinus Phase'); xlabel('angle'); ylabel('amplitude');

subplot(nbLigne3,nbColon3,3); plot(Xfreq,abs(Y2)); title('Sinus Amplitude');
subplot(nbLigne3,nbColon3,4); plot(Xfreq,angle(Y2)); title('Sinus Phase');

subplot(nbLigne3,nbColon3,5); plot(Xfreq,abs(Y3)); title('Rectangle (Thau) Amplitude');
subplot(nbLigne3,nbColon3,6); plot(Xfreq,angle(Y3)); title('Rectangle (Thau) Phase');

subplot(nbLigne3,nbColon3,7); plot(Xfreq,abs(Y4)); title('Exp avec �chelon unit� Amplitude');
subplot(nbLigne3,nbColon3,8); plot(Xfreq,angle(Y4)); title('Exp avec �chelon unit� Phase');

subplot(nbLigne3,nbColon3,9); plot(Xfreq,abs(Y5)); title('Exp complexe Amplitude');
subplot(nbLigne3,nbColon3,10); plot(Xfreq,angle(Y5)); title('Exp complexe Phase');

subplot(nbLigne3,nbColon3,11); plot(Xfreq,abs(Y6)); title('Gausienne Amplitude');
subplot(nbLigne3,nbColon3,12); plot(Xfreq,angle(Y6)); title('Gausienne Phase');

%% Comparaison entre fonction exponetielle pratique et th�orique
figure
title('Comparaison entre fonction exponetielle pratique et th�orique')
nbLigne4 = 1;
nbColon4 = 2;
subplot(nbLigne4,nbColon4,1); plot(Xfreq,real(Y6), Xfreq,imag(Y6)); title('TF Gausienne'); legend('r�elle','imaginaire');
subplot(nbLigne4,nbColon4,2); plot(Xfreq,real(Y6Theorique), Xfreq,imag(Y6Theorique)); title('TF Gausienne th�orique'); legend('r�elle','imaginaire');

%% Affichage des transform�es inverses
figure
title('Affichage des transform�es inverses')
nbLigne2 = 2;
nbColon2 = 3;
subplot(nbLigne2,nbColon2,1); plot(x,real(x1recalcule),x,imag(x1recalcule)); title('Cosinus'); xlabel('temps'); ylabel('amplitude'); legend('r�elle','imaginaire');
subplot(nbLigne2,nbColon2,2); plot(x,real(x2recalcule),x,imag(x2recalcule)); title('Sinus'); xlabel('temps'); ylabel('amplitude'); legend('r�elle','imaginaire');
subplot(nbLigne2,nbColon2,3); plot(x,real(x3recalcule),x,imag(x3recalcule)); title('Rectangle (Thau)'); xlabel('temps'); ylabel('amplitude'); legend('r�elle','imaginaire');
subplot(nbLigne2,nbColon2,4); plot(x,real(x4recalcule),x,imag(x4recalcule)); title('Exp avec �chelon unit�'); xlabel('temps'); ylabel('amplitude'); legend('r�elle','imaginaire');
subplot(nbLigne2,nbColon2,5); plot(x,real(x5recalcule),x,imag(x5recalcule)); title('Exp complexe'); xlabel('temps'); ylabel('amplitude'); legend('r�elle','imaginaire');
subplot(nbLigne2,nbColon2,6); plot(x,real(x6recalcule),x,imag(x6recalcule)); title('Gausienne'); xlabel('temps'); ylabel('amplitude'); legend('r�elle','imaginaire');

%% Affichage G300/G1000
figure
title('Affichage des transform�es inverses')
nbLigne6 = 2;
nbColon6 = 3;

subplot(nbLigne6,nbColon6,1); plot(x,real(g300),x,imag(g300)); title('g300'); xlabel('temps'); ylabel('amplitude'); legend('r�elle','imaginaire');
subplot(nbLigne6,nbColon6,2); plot(x,real(g1000),x,imag(g1000)); title('g1000'); xlabel('temps'); ylabel('amplitude'); legend('r�elle','imaginaire');

subplot(nbLigne6,nbColon6,3); plot(Xfreq,real(G300), Xfreq,imag(G300)); title('G300'); xlabel('fr�quence'); ylabel('amplitude'); legend('r�elle','imaginaire');
subplot(nbLigne6,nbColon6,4); plot(Xfreq,real(G1000), Xfreq,imag(G1000)); title('G1000'); xlabel('fr�quence'); ylabel('amplitude'); legend('r�elle','imaginaire');

subplot(nbLigne6,nbColon6,5); plot(x,real(g300recalcule),x,imag(g300recalcule)); title('g300recalcule'); xlabel('temps'); ylabel('amplitude'); legend('r�elle','imaginaire');
subplot(nbLigne6,nbColon6,6); plot(x,real(g1000recalcule),x,imag(g1000recalcule)); title('g1000recalcule'); xlabel('temps'); ylabel('amplitude'); legend('r�elle','imaginaire');

%% Affichage image
figure;
nbLigne7 = 2;
nbColon7 = 2;
subplot(nbLigne7,nbColon7,1); image(Im1); title('power(10,-5)'); colormap(gray);
subplot(nbLigne7,nbColon7,2); image(Im2); title('power(10,-4)'); colormap(gray);
subplot(nbLigne7,nbColon7,3); image(Im3); title('power(10,-3)'); colormap(gray);
subplot(nbLigne7,nbColon7,4); image(Im4); title('power(10,-2)'); colormap(gray);

%% On retrouve bien le nombre d'�chantillion dans l'int�grale
sum(Y6)